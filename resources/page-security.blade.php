@extends('layouts.app') @section('content') @include('partials.page-header')

<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="@asset('images/home-cctv.jpg')" class="img-fluid" alt="Home CCTV">
            <div class="carousel-caption d-none d-sm-block mx-3 px-5">
                <h3>CCTV Installation, Maintenance, Repair and Upgrades in Leinster</h3>
            </div>
        </div>

    </div>
</div>

<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">Security</h1>
        <p class="text-uppercase">High Definition CCTV Systems</p>
        <p class="lead">We carry out installation of security camera systems internally and externally to provide you with peace of mind
            on a day to day basis when you cannot be at your property, or while you and your family are asleep.</p>
        <hr class="my-4">
        <p>We are extremely discreet and professional and can help you with planning your home security camera system.</p>
        @include('partials.call-to-action')
    </div>
</div>

<div class="container">

<img src="@asset('images/cctv-cameras.png')" alt="" class="img-fluid">

<section>
    <div class="cctv-features">
        <h4>
            <i class="fa fa-mobile mr-2"></i>Remote Viewing</h4>
        All our camera systems can be linked up to a smartphone allowing live and recorded images to be viewed remotely.

        <h4>
            <i class="fa fa-check mr-2"></i>Easy</h4>
        Our staff are very knowledgeable and helpful and we have been providing these services to the Dublin area for over twenty
        years.

        <h4><i class="fa fa-camera mr-2"></i>Covert Cameras</h4>
        <p>If you have any security or trust issues inside the home we can install covert cameras to reassure your concerns.</p>
    </div>
</section>

    <img src="@asset('images/home-cctv-breakin.png')" alt="home break in" class="img-fluid shadow">

<section>
    <h4 class="text-uppercase mb-5">3 advantages of using a CCTV system at your home</h4>
    <h5>
        <i class="fa fa-check mr-2 text-uppercase"></i>Crime deterrent</h5>

    <p>It goes without saying that having a CCTV camera installed at your home will act as a serious deterrent to criminals
        and anyone carrying out illegal activities. The sight of a CCTV camera infers an air of danger and the presence of
        the law, deterring anyone planning to carry out a crime from doing so.</p>

    <h5>
        <i class="fa fa-check mr-2 text-uppercase"></i>Monitor activities</h5>

    <p>CCTV systems are able to keep track of what is happening at the premises where they are installed. By monitoring the
        activity of all visitors you can have total peace of mind about exactly what is going on under your roof.</p>

    <h5>
        <i class="fa fa-check mr-2 text-uppercase"></i>Collect evidence</h5>

    <p>In the unfortunate event of a crime occurring at home, having a CCTV system really does pay dividends as it provides
        a way of collecting evidence to help ‘suss out’ exactly what happened. Crimes can be solved far more easily with
        additional evidence from a CCTV camera, helping place times, locations and, most importantly, suspects.</p>
</section>
<section>

<div class="row">

<div class="col-md-6">

<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/cap3fzXGHcU"></iframe>
</div>


</div>
<div class="col-md-6">



<img src="@asset('images/high-def-cctv.png')" class="img-fluid shadow" alt="Home CCTV">
</div>
    @include('partials.call-to-action')
</section>

</div>


@endsection
