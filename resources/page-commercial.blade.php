@extends('layouts.app') @section('content') @include('partials.page-header')
<img class="card-img-top img-fluid" src="@asset('images/commercial.jpg')">
<!-- <img src="@asset('images/conference-center.png')" alt="" class="img-fluid"> -->

<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">Commercial</h1>
        <p class="lead">At AV Solutions we have an experienced, knowledgeable and creative team of employees who thrive upon providing a bespoke solution to any of your commercial AV requirements.</p>
        <hr class="my-4">
        <p>We strive on being able to deliver exceptional services and a truly bespoke solutions to both the domestic and commercial market. We treat every project individually, designed specifically to meet your needs. Our team of experienced engineers and project managers who are responsible for delivering your project on time and budget, using products chosen from our network of exclusive partners to ensure the installation is of the highest quality and includes all the latest technology.</p>
        @include('partials.call-to-action')

    </div>
</div>
<!-- <div class="container">
    <p>From Lighting hire, PA systems, microphones, tablets or screens and projectors for a small meeting, to all of your AV
        hire requirements for larger scale events such as conferences, award ceremonies &amp; product launches which may include
        staging or creative set design, and even carpeting or furniture to match your company branding. we will ensure your
        event is a success.</p>
    <p>Our team of experts pride themselves on an excellent customer service with a friendly can-do attitude. We can guarantee
        you will receive good solid technical advice on the equipment you will need, as well as any cost savings that can
        be made.</p>

    <p>AV Solutions also provides a dedicated project manager to carry out a free site visit for larger events which will require
        more intricate planning to ensure your event is a success.</p> -->

<div class="container">
<section>

<h3>VIDEO WALL INSTALLATION</h3>
<p>
    Our team of specialists at AV Solutions are well experienced at fitting video walls into any setting, weather it be an office or a business reception area. we offer landscape or portrait, large, small or medium video walls.
</p>    
<p>We work with leading manufacturers including, Samsung, NEC, Sony, we can also help with processors, content and the correct type of brackets allowing easy maintenance.</p>

<p>
    Typical uses for video walls are retail shops, meeting rooms, conference rooms, hotels, shop windows, fashion house, museums, shopping centers and more.

</p>    
<p>
    We also offer long term and short term rental solutions.

</p>   
<p>
    Please call us for more information on video wall installation on 01-2870055

</p>    
</section>


<section>
<h3>HOUSES OF WORSHIP</h3>
<p>In a place of Worship there are many determining factors for selecting the right PA and Visual combination for a specific site. We can create systems that suit your current usage and future expansion, along with the level of experience of your in-house operators.</p>

<p>Whether you are moving into a new building or updating your current worn-out equipment, we are focused on delivering a solution that suits your budget.</p>

<p>Every enquiry begins with a conversation with one of installation professionals. They will discuss your needs, ideas and time-scale for installation with the view to making recommendations based on your specifications and their experience.</p>

</section>

<section>
<img src="@asset('images/bars.png')" alt="" class="img-fluid my-4">


<h3>BARS, RESTAURANTS & RETAIL OUTLETS</h3>
<p>AV Solutions’s team fully understands that your bar, restaurant or retail outlet is a personal place which you want to transform to allow the customer/consumer a chance to feel comfortable and take in on what’s on offer as well as an exciting and unique experience. We know that you want a team you can trust, when making decisions in areas that you may not necessarily feel confident that you have a great deal of technical experience.</p>

<p>So our approach has always been to see your business with fresh eyes and offer new solutions. All you need is a basic idea of what you require and we will develop this into a working proposal. Every enquiry begins with a conversation with one of installation professionals. They are here to discuss your needs, ideas and time-scale for installation with the view to making recommendations based on your specifications and their experience.</p>

</section>

<section>

<h3>OFFICE AND COMMERCIAL SITES</h3>
<p>At AV Solutions we have been privileged to work with unique business environments with a wide variety of requests. We understand that you want a team you can trust, when making decisions in areas that you may not necessarily feel confident that you have a great deal of technical experience. So our approach has always been to see your business setting with fresh eyes and offer fresh solutions. All you need is a basic idea of what you require and we will develop this into a working proposal.</p>

<p>Every enquiry begins with a conversation with one of installation professionals. They are here to discuss your needs, ideas and time-scale for installation with the view to making recommendations based on your specifications and their experience.</p>


</section>

<section>
<img src="@asset('images/conference-centers.png')" alt="" class="img-fluid">
<h3>BOARDROOM AND CONFERENCE CENTRES</h3>

<p>With AV Solutions you can make the most dynamic use of your meeting space. We are aware that engaging your audience for pitching a proposal or providing training is vital when delivering your message.</p>

<p>Multimedia tools are now the norm when connecting with your key players so our range of audio visual solutions deliver a seamless sophisticated solutionin style with no technical expertise on your behalf required.</p>

</section>


<section>

<h3>CONFERENCE SOUND SYSTEMS</h3>
<p>AV Solutions have worked on a variety of conferences, from Education Sectors, Political, Local committees to Environmental Conferences. We understand that the audiences are focused on the chair of the conference, so sound is imperative to deliver optimum results to get important points across to the panel and audience.</p>

<p>The sound system set up for your conference will be dependent on the type of room and the sort of conference you are holding. Things to consider for the set up are, if it will one to one talks, open discussions or panel discussions.</p>

<p>Another factor to consider is if you prefer a wired or wireless system. Wireless has more benefits for hire purposes, as it will just be the case of set up and take down of the sound equipment. If you require a permanent set up, our team can arrange an installation for your conference room.</p>


</section>

<section>

<h3>THEATERS AND COMMUNITY CENTRES</h3>
<p>At AV Solutions we understand the responsibilities community services have and also the impact they have on the local people. We ensure that the investment you make in the audio visual equipment you choose to instal is right for your needs and fits your budget with the view to standing the test of time. Every enquiry begins with a conversation with one of installation professionals.</p>

<p>They are here to discuss your needs, ideas and time-scale for installation with the view to making recommendations based on your specifications and their experience.We follow up with a site-visit at a time to suit you; this is an opportunity to check the practical elements of the installation and fine-tune elements necessary in order to put together a complete quotation. We are available for any questions following the quotation phase in order to help you make the right decision for your theatre or community service. We understand that it is important for solutions to be practical from basic sound systems suitable for local assembly halls and local sports events, to larger fixed sound systems to cater for drama and music productions.
</p>

<p>The installation itself is scheduled at a time to suit you; once the installation is complete, we offer comprehensive training with the view to helping you to make the most of your AV system. This includes the basic operation of audio and lighting mixing desks, wireless microphones, DJ equipment etc Thus ensuring the optimum performance of your system and making every event or production a roaring success. Once the installation is complete, we also offer a maintenance service.</p>

</section>


<section>
    
    <div class="row my-4">
        <div class="col-md-6">
            <img src="@asset('images/support.png')" alt="" class="img-fluid">
        </div>
        <div class="col-md-6">
            <h3>ASSISTED LISTING </h3>
        </div>
    </div>
    
</section>

<section>

    <!-- <div class="row my-4">
        <div class="col-md-6">
            <img src="@asset('images/bars.png')" alt="" class="img-fluid">
        </div>
        <div class="col-md-6">
            <h3>BARS, RESTAURANTS & RETAIL OUTLETS</h3>
        </div>
    </div> -->
</section>


<h3>Our products include:</h3>
<ul class="list-group">
    <li class="list-group-item">Projector screens</li>
    <li class="list-group-item">LCD & Plasma screens</li>
    <li class="list-group-item">Projectors, AV mixers and editing tools</li>
    <li class="list-group-item">CD & DVD players</li>
    <li class="list-group-item">Portable and fixed Sound Systems</li>
    <li class="list-group-item">Wireless microphone systems</li>
    <li class="list-group-item">Lecterns and Lectern microphones</li>
    <li class="list-group-item">Audio and lighting mixing consoles</li>
    <li class="list-group-item">Theatre, stage & effect lighting</li>
    <li class="list-group-item">Staging</li>
    <li class="list-group-item">100 volt line outdoor systems</li>
    <li class="list-group-item">Induction loop systems</li>
    <li class="list-group-item">Comprehensive training</li>
</ul>


    <p class="lead mt-5">Please call for advice or a free no hassle quotation.</p>
    @include('partials.call-to-action')

</div>
</div>


@endsection
