@extends('layouts.app') @section('content') @include('partials.page-header')

<img src="@asset('images/home-automation.jpg')" alt="" class="img-fluid">
<div class="jumbotron jumbotron-fluid text-light bg-dark">
    <div class="container">
        <h1 class="page-title">Home Automation</h1>
        <p class="lead page-desc">Merge all of your home technologies such as entertainment, comfort &amp; security systems into a single, easy to use smart system.</p>
        <p>You and your family have total control over your home's technology through sleek and intuitive user interfaces such as handheld remotes, touch panels &amp; mobile devices such as Apple &amp; Android.
        </p>
    </div>
</div>
<div class="container">
    <div class="embed-responsive embed-responsive-16by9 mb-5">
        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Mmo5YQbVxH4" allowfullscreen></iframe>
    </div>
    <h2>Welcome to the future</h2>
    <p>Enjoy the ultimate convenience of a custom-tailored Smart Home that is simple to use, simply select what you want to watch and the system will set up the entire room, removing the need for all those horrid remotes, or press the <em>Away</em> button and the system turns off all the lights, TVs &amp; music systems, adjusts the temperature, sets the security system to full alarm, tells your automatic vacuum cleaner to start cleaning the house and sends you a message confirming the home is secure.</p>
    @include('partials.call-to-action')    
</div>

@endsection
