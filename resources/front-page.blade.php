@extends('layouts.app') 
@section('content') 
@include('partials.page-header')
@include('partials.primary-slider')


<section class="banner">
    <div class="container mt-4 mb-4">
        <h1>Audio Visual Systems Integration</h1>
        <p class="lead">Consultancy, design, supply, installation and support of audio and audiovisual systems.</p>
        </p>
         

    @include('partials.call-to-action')
    </div>
</section>

@endsection
