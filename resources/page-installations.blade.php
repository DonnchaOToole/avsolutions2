@extends('layouts.app') @section('content') @include('partials.page-header')


<img src="@asset('images/outdoor.jpg')" alt="" class="img-fluid">
<div class="jumbotron jumbotron-fluid text-light bg-dark">
    <div class="container">
        <h1 class="page-title">Installlations</h1>
        <p class="lead page-desc">Take the frustration out of setting up your system with our installation service.</p>
    </div>
</div>
<div class="container">
    <p class="lead">With so many audio visual products on the market it is often hard to decide what will suffice in an installation so we
        take all that guess work away from our clients by installing a system that will be reliable, cost effective and user
        friendly.
    </p>
    <p>At AV Solutions, we are able to select equipment depending on requirement, specification and performance, as we are not
        aligned to any specific manufacturer.</p>

    @include('partials.call-to-action')

    <div class="row my-5">
        <div class="col-md-4">
            <h4>Copper</h4>
            <p>AVSolutions install structured cabling systems from some of the world's premium brands, CAT5e, CAT6 &amp; CAT6a
                solutions, all supplied with a 25 year performance warranty.</p>
        </div>
        <div class="col-md-4">
            <h4>Fibre</h4>
            <p>AVSolutions are specialists in the installation of fibre optic cabling providing both conventional &amp; blown
                fibre, including fibre to the desk and pre-terminated options.</p>
        </div>
        <div class="col-md-4">
            <h4>WiFi</h4>
            <p>AVSolutions can offer you the latest in WiFi design &amp; installation from some of the leading brands in the
                market. Business, Retail, Education or home we have a WiFi solution just right for you.</p>
        </div>
    </div>

    <div class="py-4">
    <h4>Our Process</h4>
    <p>Our implementation team works closely with the design team and third parties to make sure that the systems we implement
        meet high quality standards and meet tight deadlines.</p>
    <p>We will also provide you with regular updates throughout the delivery process. You will receive a Project Completion
        Pack in both paper and electronic format (where applicable). This will contain the relevant project specific documentation,
        designs and custom configuration files.</p>
    <p>Whether your requirement is for a large screen TV installation in a meeting room or reception area, projector and electric
        screen install in a boardroom or a multi TV screen install in a call centre, we can offer you the best possible solution.
    </p>

    <!-- <img src="https://placehold.it/1280x720" alt="" class="mt-2 mb-2 img-fluid"> -->
    <p>One of our most popular home installations is a system that provides top quality music throughout the home.</p>
    <p>Carefully installed without a wire to be seen, a multi-room system will let you listen to music wherever you have a speaker
        plus you can all be listening to different sources at the same time: streamed music from Spotify or internet radio.</p>

    </div>
<div class="py-4">
    <h4>Music &amp; Movies</h4>
    <p>Our installation experts will not only advise on systems that will suit you, but we will also help you choose the best
        system for accessing your music and movies. This is an ever-changing game, with hi-res music and Ultra HD streamed
        movies becoming more accessible and affordable.</p>
    <ul class="list-group">
        <li class="list-group-item">
            <i class="fa fa-check"></i>Multi Room Installations</li>
        <li class="list-group-item">
            <i class="fa fa-check"></i>In-wall or Ceiling Speaker Installation</li>
        <li class="list-group-item">
            <i class="fa fa-check"></i>Outdoor Speakers</li>
        <li class="list-group-item">
            <i class="fa fa-check"></i>Surround Sound</li>
    </ul>
</div>

<div class="py-4">
    <h4>Wireless vs Wires</h4>
    <p>Wireless is great but for the ultimate in quality, it is still difficult to beat a cabled system even if we do have to
        cut a few holes in the walls to hide them. Sure, a wired system is more complex and costly to install but if you
        want quality over convenience, then going wired is a sensible option. What's more, we guarantee that any renovation
        to the home, such as where we have had to run cables through walls or under the floor, will be put back as good as
        new.</p>

    <p>Many of us dream of a dedicated home cinema and AV Solutions can transform your living room or basement into cinema-style
        surround sound with a hi definition projector on a giant screen, nicely styled seating and subtle lighting, you won't
        find a reason to ever go to your local multiplex again.</p>
    <ul class="list-group">
        <li class="list-group-item">
            <i class="fa fa-check"></i>Screen Installations</li>
        <li class="list-group-item">
            <i class="fa fa-check"></i>Projector Installations</li>
        <li class="list-group-item">
            <i class="fa fa-check"></i>Surround Sound Installations</li>
        <li class="list-group-item">
            <i class="fa fa-check"></i>Wireless Systems</li>
    </ul>
</div>
<div class="py-4">
    <h4>Smart Lines</h4>
            <p>Gone are the days that when you needed to have the AV Equipment taking up valuable space in the same room as
                your screen.</p>
            <p>Home cinema control systems are sophisticated and can be hidden away out of sight, in a basement or if you prefer,
                or in some attractive built for the purpose, furniture. Everything can be controlled from your Smartphone,
                tablet or touch pad.</p>
    
</div>
<div class="py-4">
    <h4>Speaker Design</h4>
            <p>Whether you decide to go for a TV on the living room wall or a sophisticated dedicated cinema room, there are
                a wealth of speaker combinations that we can install – 3, 5, 7 or even 11.</p>
            <p>One of our most popular arrays includes Ceiling speakers, they look the part and are incredibly effective with
                the addition of Dolby Atmos.</p>
    
</div>

<div class="py-4">
    <h4>Dolby ATMOS and DTS:X: The New Immersive Technologies</h4>
            <p>The new Dolby Atmos and DTS:X surround sound formats add overhead sound effects to your home theatre. Both deliver
                surround sound with far greater realism than previous formats but the set up is complex employing an array
                of up to 11 speakers; This is one very good reason why it's always worth getting the job done profession.</p>
    
</div>
    @include('partials.call-to-action')
</div>

@endsection
