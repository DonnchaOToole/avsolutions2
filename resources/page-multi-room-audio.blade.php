@extends('layouts.app') @section('content') @include('partials.page-header')

<img src="@asset('images/speakers-room.png')" alt="" class="img-fluid">
<div class="jumbotron jumbotron-fluid">
    <div class="multi-room-intro container my-5">
        <h1>Multi-Room Audio</h1>
        <h2 class="text-muted">Your favorite music in every room.</h2>
        <p class="lead"> Music is relaxing and can reduce stress. Why not create moments of happiness in your home through music? Wake up
            to your favorite song in the morning with a sonos programmed alarm, listen to relaxing music while in the shower
            after a long day at work.</p>
    </div>

</div>

<div class="container">
    <div class="my-3 embed-responsive embed-responsive-16by9 mb-3">
        <iframe src="https://www.youtube.com/embed/tDWTWDQTdZQ?rel=0&amp;showinfo=0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>

    <!-- <img src="@asset('images/hero6.jpg')" alt="" class="img-fluid"> -->
    <p class="lead">With a dedicated multi-room audio system, sound from numerous sources (hard drive, online music subscription services)
        can be played throughout your home and controlled through your smartphone or tablet.</p>


    <p>The wide range of multi-room audio products on the market offer solutions for virtually any scenario, and we can advise
        on the product, or products, that will best suit your needs; whether this means compatibility with existing technology
        you own, or the need for sound to work within a space that has limited acoustics.
    </p>
    <p>The multi-room audio system that’s best for your property will partly depend on the size and shape of your rooms, the
        position you wish to place your speaker(s) and your longer-term plans for any further home designs or budget.
    </p>
    <div class="embed-responsive embed-responsive-16by9 my-5">
        <iframe src="https://www.youtube.com/embed/XStNHZSXfUU?rel=0&amp;showinfo=0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>
    <h3>Controlled wirelessly and integrated throughout your home.</h3>
    <p>Often we will recommend the amazing Sonos system,but there are other products that may be equally as well suited too.
        Whichever system is selected, you won’t be stuck with ugly wires, boxes and cables all over your house – your multi-room
        audio system is based around a speaker (or system of integrated speakers) that communicates wirelessly with your
        Smartphone or tablet.</p>
    <img src="@asset('images/fire-room.png')" alt="" class="my-5 img-fluid">
    
    <div class="row">
        <div class="col-md-6">
    <p>Your speakers, along with any ceiling speakers and AMPs, can be programmed to receive audio, when directed, through an
        app on your mobile device. The more speakers you have, the more locations you can send sound to. What’s more, you
        can vary what you listen to throughout the house. Simply open your mobile app, select your room and choose what you
        would like to play there.</p>
        <!-- <img src="@asset('images/image17.png')" class="img-fluid my-3 shadow" alt="Home CCTV"> -->
        </div>
        <div class="col-md-6">
        <img src="@asset('images/image18.png')" class="img-fluid my-3 shadow" alt="Home CCTV">
        </div>
    </div>
        <img src="@asset('images/image19.png')" class="img-fluid my-3 shadow" alt="Home CCTV">
        <img src="@asset('images/image20.png')" class="img-fluid my-3 shadow" alt="Home CCTV">
        <img src="@asset('images/image21.png')" class="img-fluid my-3 shadow" alt="Home CCTV">
        <img src="@asset('images/image22.png')" class="img-fluid my-3 shadow" alt="Home CCTV">
        <img src="@asset('images/image23.png')" class="img-fluid my-3 shadow" alt="Home CCTV">

<section>
    <h3>Project Management</h3>
    <p>When filling a home with music it is incredibly important to plan the project correctly. If done during the original
        design stage, we will be able to go through the project with the architects and electricians and ensure everyone
        is in agreement and knows exactly what is to be done.</p>
    <p>It is vital for this type of project to get an experienced installer, once the cables have been run it can be very difficult
        to change the plans. </p>
    <p>We have done hundreds of multi-room audio projects in Dublin and the Ireland for over 25 years. Its this experience that
        helps us make sure everything is planned for and executed without any issues.</p>
</section>
<section>
    <h3>Sonos</h3>
    <div class="row">
        <div class="col-sm-6">
            <div class="embed-responsive embed-responsive-16by9 mb-5">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2ha8PG4GAKA" allowfullscreen></iframe>
            </div>

        </div>
        <div class="col-sm-6">
            <div class="embed-responsive embed-responsive-16by9 mb-5">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/AMnxiaQPtBs" allowfullscreen></iframe>
            </div>

        </div>
    </div>
</section>
    @include('partials.call-to-action')
</div>

@endsection
