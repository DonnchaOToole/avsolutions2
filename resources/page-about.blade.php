@extends('layouts.app')

@section('content')
  @include('partials.page-header')

<div class="container">
    <img src="@asset('images/hero6.jpg')" alt="" class="img-fluid">
    <h1>AV Solutions</h1>
    <p class="lead">Celebrating over 20 years of audio visual services.</p>
<p>Since 1996, AV Solutions having been providing and
installing high quality audio visual products and services, from state of the art home and commercial multiroom
sound systems to home cinema and digital display.
</p>
<p>To learn more about what we have to offer, get in touch
with us today.</p>
@include('partials.call-to-action')

</div>

@endsection