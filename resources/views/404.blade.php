@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, but the page you were trying to view does not exist.', 'sage') }}
    </div>
    <div class="container">
    <h3>We can't find that page</h3>
    <p>Click <a href="<?php echo get_home_url();?>">here</a> to go to the home page. </p>
    </div>
  @endif
@endsection
