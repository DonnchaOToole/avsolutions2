<!-- <div class="row logo-header">
  <div class="col-sm-4">
  </div>
  <div class="col-sm-4">
  <div class="text-center">
  <a href="{{ home_url('/') }}">
    <img src="@asset('images/logo.svg')" class="img-fluid">
  </a>
  <h2>AV Solutions</h2>
  <div class="smart-home-company">
  <p class="align-middle"><img src="@asset('images/smart-house-logo.svg')" class="img-fluid power-house align-middle">
The Smart Home Company</p>
  </div>
</div>
  
  </div>
</div> -->
<div class="top-strip py-2">
  <div class="container">
    <span class="phone-number text-white text-right"><i class="fa fa-phone mr-2"></i>+353 (0) 01 287 0055</span>
  </div>
</div>

<nav class="navbar navbar-expand-xl navbar-dark bg-dark">
  <a class="navbar-brand" href="{{ home_url('/') }}">
    <img src="@asset('images/logo.svg')" width='30' height='30' class="brand-logo d-inline-block align-top mr-1">
    <span class="brand-name">AVSolutions</span>
    <p class="smart-home-tag">The
      <span class="smart">Smart</span> Home Company</p>
  </a>
  <!-- <img src="@asset('images/smart-house-logo.svg')" width='50' height='50' class="brand-logo d-inline-block align-top mr-1 visible-lg"> -->
  
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
    aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="{{ home_url('/what-we-do/') }}">What We Do</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ home_url('/commercial/') }}">Commercial</a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link" href="{{ home_url('/multi-room-audio/') }}">Multi-Room Audio</a>
      </li>
      <!-- <li class="nav-item">
        <a class="nav-link" href="{{ home_url('/contact/') }}installations/">Installations</a>
      </li> -->
      <li class="nav-item">
        <a class="nav-link" href="{{ home_url('/home-cinema/') }}">Home Cinema</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ home_url('/security/') }}">Security</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ home_url('/home-automation/') }}" />Home Automation</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ home_url('/contact/') }}">Contact</a>
      </li>
    </ul>
  </div>
</nav>
