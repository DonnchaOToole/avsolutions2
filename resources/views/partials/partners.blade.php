<div class="container pt-5">
<h3 class="text-center my-5">Partners</h3>
    <div class="row py-4">
        <div class="col"><img src="@asset('images/yamaha_logo.jpg')" alt="" class="img-fluid partner-logo"></div>
        <div class="col"><img src="@asset('images/control-four-logo.jpg')" alt="" class="img-fluid partner-logo"></div>
        <div class="col"><img src="@asset('images/artcoustic-logo.jpg')" alt="" class="img-fluid partner-logo"></div>
        <div class="col"><img src="@asset('images/bluestream-logo.jpg')" alt="" class="img-fluid partner-logo"></div>
        <div class="col"><img src="@asset('images/sonos_approved.png')" alt="" class="img-fluid partner-logo"></div>
        <div class="col"><img src="@asset('images/lutron-logo.jpg')" alt="" class="img-fluid partner-logo"></div>
    </div>
    <div class="row py-4">
        <div class="col"><img src="@asset('images/keff-logo.png')" alt="" class="img-fluid partner-logo"></div>
        <div class="col"><img src="@asset('images/logo-lg.jpg')" alt="" class="img-fluid partner-logo"></div>
        <div class="col"><img src="@asset('images/ubiqiti-logo.jpg')" alt="" class="img-fluid partner-logo"></div>
        <div class="col"><img src="@asset('images/soundcraft-logo.png')" alt="" class="img-fluid partner-logo"></div>
        <div class="col"><img src="@asset('images/artcoustic-logo.jpg')" alt="" class="img-fluid partner-logo"></div>
        <div class="col"><img src="@asset('images/Ruckus-logo.jpg')" alt="" class="img-fluid partner-logo"></div>
    </div>
</div>