<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="@asset('images/hero3.jpg')" alt="Installations">
      <div class="carousel-caption d-none d-md-block">
        <h2 class="display-2">Installations</h2>
        <p>Take the frustration out of setting up your system with our installation service.</p>
      </div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="@asset('images/bad-sound.jpeg')" alt="Home Audio">
      <div class="carousel-caption d-none d-md-block">
        <h2 class="display-2">Multi-room Audio</h2>
        <p class="lead">Had enough of bad sound?</p>
      </div>
    </div>
    
    <div class="carousel-item">
      <img class="d-block w-100" src="@asset('images/hero4.jpg')" alt="Home Cinema">
      <div class="carousel-caption d-none d-md-block">
        <h2 class="display-2">Home Cinema</h2>
        <p class="lead">You can have the big screen cinema experience in your own home.</p>
      </div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="@asset('images/hero5.png')" alt="TV Wall Mounting">
      <div class="carousel-caption d-none d-md-block">
        <h2 class="display-2">From Concept to Production </h2>
        <!-- <p class="lead">We can install any size TV to any wall.<br><i class="fa fa-caret-right mr-2"></i>Ask us about our soundbar installation too.</p> -->
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
