<div class="container my-5">
  <div class="row">
    <div class="col-sm-6">
      <h3>System Design and Planning</h3>
      <p>AV Solutions take our installations very seriously.</p>
      <p>We have trained system designers that work closely with architects
        and property developers to make sure you achieve the most out of your property when it comes to audio and visual.</p><p>
        We listen to and work with you to make sure you are happy every step of the way.</p>
    </div>
    <div class="col-sm-6">
      <img src="@asset('images/plans.png')" alt="System Design and Planning" class="img-fluid">
    </div>
  </div>
</div>
