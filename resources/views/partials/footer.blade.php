@include('partials.system-design')
@include('partials.partners')
<section class="cta pt-2 mb-0 mt-5">
  <div class="jumbotron mb-0">
    <div class="container">
      <h2>Get in touch today</h2>
      <p class="lead">Open 9am - 5pm, Monday to Friday</p>
      <ul class="footer-list">
        <li><i class="fa fa-phone mr-2"></i>+353 (0) 01 287 0055</li>
      </ul>
      <a href="<?php echo get_home_url();?>/contact/" class="btn btn-lg btn-primary">
      <i class="fas fa-envelope mr-2"></i>
      Message</a>
      <a href="tel:00353872528777" class="btn btn-lg btn-primary">
      <i class="fas fa-phone mr-2"></i>
      Call</a>
    </div>
  </div>
  </div>
</section>
<footer>
      <div class="footer-text text-center pt-2 pb-2 text-uppercase">
    <div class="container">
        AVSolutions.ie &copy; <?php echo date("Y"); ?> |
        <a rel='nofollow' href="https://focalise.ie">Website by Focalise</a>
      </div>
    </div>
</footer>
