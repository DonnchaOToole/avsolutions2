<div class="call-out-bg">
<div class="container my-5">
  <div class="row">
    <div class="col-sm-6">
      <h3>Keeping You Online</h3>
      <p class="lead">Proactive and reactive maintenance plans for your AV systems</p>
      <p>As a company we pride ourselves on our customer service and satisfaction, product knowledge and experience. This helps us to ensure we provide our customers the highest quality products with service to match there needs.</p>
      <p>Whether you have a party planned and your sound system has gone down or 
your TV is not working the AV Solutions team is here to help.
</p>
    <a href="tel:0035312870055" class="btn btn-primary"><i class="fa fa-phone mr-2"></i>Call Us Now</a>
    </div>
    <div class="col-sm-6">
      <img class="img-fluid" src="@asset('images/lc.png')" alt="">    
    </div>
  </div>
</div>
</div>
