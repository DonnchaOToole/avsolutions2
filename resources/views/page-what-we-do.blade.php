@extends('layouts.app') @section('content')

<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">What We Do</h1>
    <p class="lead">We offer a wide range of bespoke solutions for both the domestic and commercial markets, for all your audio and visual needs. 
</p>
  </div>
</div>

<div class="container">

<div class="row">
  <div class="col-sm-6">
  <img src="@asset('images/on_air.jpg')" alt="" class="img-fluid">
  </div>
  <div class="col-sm-6">
    <p class="lead">Whether it’s a smart TV that needs connecting or you want WiFi in the office at the bottom of the garden we are the company for you.</p>
    <p class="lead">Our engineers are trained in all aspects of home AV and have at least 20 years experience in the industry.</p>  
  </div>
</div>




<div class="row my-5 py-5 what-we-do-grid text-center">
  <div class="col-md-3">
    <i class="far fa-5x my-3 grid-icon fa-lightbulb"></i>
    <h4>got an idea?</h4>
    <p>Everything starts with a idea and we are here to turn that idea into a reality.</p>
  </div>
  <div class="col-md-3">
    <i class="fa fa-5x my-3 grid-icon fa-columns"></i>
    <h4>The drawing board</h4>
    <p>We look at your idea and work out the best way to achieve this at the best price possible.</p>
  </div>
  <div class="col-md-3">
    <i class="fa fa-5x my-3 grid-icon fa-pencil-alt"></i>
    <h4>Quotation</h4>
    <p>Our highly skilled AV consultants will write up a hassle free quotation free of charge.</p>
  </div>
  <div class="col-md-3">
    <i class="fa fa-5x my-3 grid-icon fa-comments"></i>
    <h4>Consulting</h4>
    <p>We will discuss your options and arrange an installation date that suits you.</p>
  </div>
</div>

<div class="objective my-5">
<h4>Company Objective</h4>
<p class="lead">
"To deliver to our clients a personalised audio visual system that will eclipse their expectations but not their budget"
</p>
</div>

  <div class="row mb-4">
    <div class="col-sm-6">
      <img src="@asset('images/networking.png')" alt="" class="img-fluid">
    </div>
    <div class="col-sm-6">
      <h3>WiFi &amp; Networking</h3>
      <p>We have years of experience to make sure only the latest high speed data network is installed into your home to form
        the very back bone that all smart homes require. By creating a stable wired and wireless network in your home, you
        can be assured of full connectivity that is vital to you.</p>
      <p>Our Installers have undergone years of experience installing networks so you can be sure your devices are connected
        reliably and seamlessly throughout your home, garden or wherever you need.</p>
    </div>
  </div>

  <div class="row mb-4">
    <div class="col-sm-6">
      <img src="@asset('images/high-def-cctv.png')" alt="" class="img-fluid">
    </div>
    <div class="col-sm-6">
      <h3>HIGH DEFINITION CCTV</h3>
      <p>At AVSolutions we understand that home security is very important and that’s why we offer the very best in CCTV
        quality. Our solutions integrate seamlessly into our control system so you can see your cameras from either your
        Television or iPhone/iPad.</p>
      <p>We’ll not only carry out the CCTV installations but plan, design and provide advice on integrating CCTV technology
        into your home – having great quality cameras are important but focusing on areas that are most vulnerable is key
        to protecting your home.</p>
    </div>
  </div>

  <div class="row mb-4">
    <div class="col-sm-6">
      <img src="@asset('images/home-cinema.png')" alt="" class="img-fluid">

    </div>
    <div class="col-sm-6">
      <h3>HOME CINEMA</h3>
      <p>Our expert team design, project manage and install a wide range of specialist home theatre systems and aim to give
        you the best cinema experience possible. Whether you would like a discreet cinema system that disappears into your
        existing space or create a dedicated cinema room, we can provide a solution for you.</p>
      <p>We will expertly design a system suitable for your room size that will give you an immersive cinema experience of fantastic
        High Definition Picture and sound. We choose carefully the equipment that will complement each other and cleverly
        calibrate it to give you optimum sound performance.</p>
    </div>
  </div>
  <div class="row mb-4">
    <div class="col-sm-6">
      <img src="@asset('images/soundbar-mounting.jpg')" alt="" class="img-fluid">
    </div>
    <div class="col-sm-6">
      <h3>TV &amp; SOUNDBAR WALL MOUNTING</h3>
      <p>Modern day televisions are usually flat and lightweight which makes them ideal to mount on a wall. There apperance
        compliments modern interior design perfect and allows a neat, cable free wall mounting. Correct mounting brackets
        and technique are important when mounting expensive televisions on interiort walls. Poor mounting can risk injury
        and damage to walls which could be very costly, to get the best fitting it is best to hire a professional to wall
        mount your television.</p>
    </div>
  </div>
  <div class="row mb-4">
    <div class="col-sm-6">
      <img src="@asset('images/multi-room-audio.jpg')" alt="" class="img-fluid">

    </div>
    <div class="col-sm-6">
      <h3>MULTIROOM AUDIO</h3>
      <p>Imagine all your favourite music and movies in the highest quality in every room at touch of a button. Here at AV Solutions
        we can bring all your digital media including iTunes, Spotify and integrate it with video sources such as Sky HD,
        Virgin, Blu-ray and bring it together on one intuitive remote control.</p>


      <p>Using top manufactures like Blustream, Monitor Audio and Sonos to distribute high definition video &amp; sound, we can
        offer uncompromising quality throughout your home.</p>

      <p>Whether you are a builder, architect, developer or home owner we are here to help. We offer a complete service from
        design through to installation. On contacting us you will be assigned a project manager who will look after you all
        the way through your project.</p>
    </div>
  </div>
  <div class="row mb-4">
    <div class="col-sm-6">
      <img src="@asset('images/hd_4k.webp')" alt="" class="img-fluid">

    </div>
    <div class="col-sm-6">
      <h3>HD &amp; 4K DISTRIBUTION</h3>
      <p>Homes of the future are minimal. AV Solutions are at the forefront of HDBaseT distribution. We can install all your
        HD and 4K devices in one central location and distribute them to any number of TV's all over one Cat6 cable. Thus
        giving you viewing capabilities of all your devices at each TV position with full control. No need for SKY boxes,
        BluRay players, and CCTV Recorders sitting under TV's. All our systems are compatible with 4K. Should you require
        a commercial installtion with a high number of screen please call us for more information.</p>
    </div>
  </div>
  <div class="row mb-4">
    <div class="col-sm-6">
      <img src="@asset('images/satellite.png')" alt="" class="img-fluid">

    </div>
    <div class="col-sm-6">
      <h3>Satellite &amp; Aerial</h3>
      <p>AV Solutions install and repair all types of satellite and digital aerial antennas. Our installation and repair service
        is simple and fast, usually completed in less than 2 hours. Our technicians are insured and fully trained for the
        protection of the property and the safety of the installer.</p>

      <p>Satellites dish installation opens up a world of viewing opportunities with a huge range of satellites available there
        is something for everyone’s taste whether it’s Freesat HD, SKY HD or foreign satellites, we are experts in all aspects
        of installation.</p>

      <p>We can promise we will get you SKY even if you have been refused by SKY themselves we carry a wide range of brackets
        and equipment that allow us to get you that all important signal that SKY may have failed to achieve. Have you got
        a problem with your SKY system that they have been unable to put right? We will do everything we can we will have
        you back up and no time</p>
    </div>
  </div>
</div>
@include('partials.emergency-call-out-service')
@endsection
