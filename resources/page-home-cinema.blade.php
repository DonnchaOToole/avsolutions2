@extends('layouts.app')

@section('content')
  @include('partials.page-header')

<img src="@asset('/images/home-cinema.jpg')" alt="" class="img-fluid">
<div class="jumbotron jumbotron-fluid bg-dark text-light">
    <div class="container">
        <h1 class="page-title">Home Cinema</h1>
        <p class="lead page-desc">You can have the big screen cinema experience in your own home.
        </p>
        @include('partials.call-to-action')

    </div>
</div>
<div class="container">
    <p>With years of experience providing bespoke home cinema installations. AV Solutions have the expertise and capabilities to offer our clients an extensive range of options, creating a unique home cinema system that meets their specific requirements.</p>
    
    <img src="@asset('images/cinema-room.png')" alt="" class="img-fluid my-4">
    <p>Cinema systems and dedicated cinema rooms are becoming more and more popular in residential dwellings today. Whether it’s a large TV wall mounted on a motorised bracket or a full media room, we can offer products across the spectrum of home cinema equipment, and will design and build to meet your specific requirements. We work closely with our clients’ architects, builders and interior designers to make sure you are getting the end result you desire.
    </p>
    <img src="@asset('images/cinema-room2.png')" alt="" class="img-fluid my-4">

    <p>We specialise in creating fully integrated home cinema systems that can be controlled remotely, using your mobile devices, achieving full functionality with home entertainment devices &amp; automation systems throughout your property.
    </p>
    <img src="@asset('images/cinema-room3.png')" alt="" class="img-fluid my-4">
    

    <h2>Bespoke Home Cinema Systems </h2>
    <p>You may just want to compliment a new TV with a 5.1 surround sound system, or you may want a media room with a full home cinema system installed. We specialise in both ends of the scale and have installed small TVs for bedrooms or bathrooms as well as 7.1 surround sound systems for LED TVs with a screen size of 55” and upwards. Furthermore we can fit home cinema projectors, which are ideal for dedicated cinema or media rooms, offering an excellent big screen viewing experience. There are no limits to what AV Solutions can offer our customers when it comes to home cinema installations in Leinster.
    </p>
    <img src="@asset('images/home-cinema-4.jpg')" alt="" class="img-fluid my-4">


    <p>Our installations utilise all the latest technology from motorised recessed screens and projectors, hidden speakers, and movie management and storage systems. All of this equipment can be controlled through a range of products we sell and install to make your life that much easier. Remote controls that will do all the work for you with one touch of a button makes using a cinema system easy for even the most hardened technophobes.</p>

    <p>We also provide home cinema projector installations and servers for media centres, allowing you access to your entire video and music library from any room in your home. Not only do AV Solutions supply the home cinema equipment, but we’ll design the entire home entertainment system for you, from the wiring and selecting the best home cinema equipment such as surround sound, Blu Ray players, screens, projectors and even the furniture.
    </p>
    @include('partials.call-to-action')

</div>


@endsection