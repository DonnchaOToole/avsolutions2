@extends('layouts.app') @section('content') @include('partials.page-header')


<div class="jumbotron jumbotron-fluid text-white bg-dark">
    <div class="container">
    <div class="row">
        <div class="col-sm-6">
        <h1>Contact</h1>
        </div>
        <div class="col-sm-6">
        <h5>COMPANY OBJECTIVE</h5>
        <p>"To deliver to our clients a personalised audio visual system that will eclipse their expectations but not their budget"</p>
        </div>
    </div>
    </div>
</div>
<div class="container">
    <div class="contact-row py-5 text-center">
        <div class="row">
            <div class="col-sm-4">
                <i class="fas fa-phone fa-3x"></i>
                <h5>Give Us A Call</h5>
                <p>+353 (1) 287 0055</p>
            </div>
            <div class="col-sm-4">
                <i class="fas fa-envelope fa-3x"></i>
                <h5>General Enquiries</h5>
                <p>info@avsolutions.ie</p>
            </div>
            <div class="col-sm-4">
                <i class="fas fa-archive fa-3x"></i>
                <h5>Administration</h5>
                <p>invoices@avsolutions.ie</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <a href="tel:0035312870055" class="btn btn-light">Call Us</a>
            </div>
            <div class="col-sm-4">
                <a href="mailto:info@avsolutions.ie" class="btn btn-light">Ask Us</a>
            </div>
            <div class="col-sm-4">
                <a href="mailto:info@avsolutions.ie" class="btn btn-light">Email Us</a>
            </div>
        </div>
    </div>
    <h2>Contact Form</h2>
    <form action="https://formspree.io/info@avsolutions.ie" method="POST">
        <div class="form-group">
            <label for="name-input">
                Name
            </label>
            <input id="name-input" class="form-control" type="text" name="name" placeholder="Your name">
        </div>
        <div class="form-group">
            <label for="email-input">Email</label>
            <input id="email-input" class="form-control" type="email" name="_replyto" placeholder="Your email">
        </div>
        <div class="form-group">
            <label for="phone-input">Phone Number</label>
            <input id="phone-input" class="form-control" type="text" name="phone" placeholder="Your phone number">
        </div>
        <div class="form-group">
            <label for="enquiry-input">Enquiry</label>
            <textarea id="enquiry-input" class="form-control" type="textarea" name="enquiry" placeholder="Your enquiry"></textarea>
        </div>
        <div class="form-group">
            <input class="btn btn-primary btn-primary-outline btn-lg" type="submit" value="Send">
        </div>
    </form>
</div>



@endsection
